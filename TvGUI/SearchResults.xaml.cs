﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TvCore;
using TvCore.DataManagment;
using System.Linq;
using System.Collections;

namespace TvGUI
{
    /// <summary>
    /// Interaction logic for SearchResults.xaml
    /// </summary>
    public partial class SearchResults : Window
    {
        public List<Series> Results { get; private set; }

        public SearchResults(string term)
        {
            InitializeComponent();
            this.Title = $"Rezultati pretrage za: {term}";
            List<Series> results = Utilities.SearchShows(term);
            bool isEmpty = results == null;

            if (!isEmpty)
            {
                results.RemoveAll(s => Object.Equals(s.Image, default(TvCore.Image))); //uklanja sve fetchane serije bez slike, najcesce su iznimno nepoznate, kvari mi dizajn
                icTodoList.ItemsSource = results;
            }
            else
                icTodoList.ItemsSource = new List<Series>() { new Series() };  
         
            Term = term ?? throw new ArgumentNullException(nameof(term));
            Results = results;

        }
        private void ButtonClicked(object sender, RoutedEventArgs e)
        {
            var button = sender as Grid;
            var code = ((Series)button.DataContext).Id;
            if (code != -1)
            {
                Show show_page = new Show(code);
                this.Content = show_page;
            }
        }


        public string Term { get; private set; }
    }
}
