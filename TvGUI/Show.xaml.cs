﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TvCore;
using TvCore.DataManagment;
using System.Linq;
using System.Collections;

namespace TvGUI
{
    /// <summary>
    /// Interaction logic for Show.xaml
    /// </summary>
    public partial class Show : Page
    {
        public int Id { get; set; }
        public string SanitizedSummary { get; set; }
        public Show(int id)
        {
            InitializeComponent();
            Id = id;
            this.Loaded += new RoutedEventHandler(ShowLoaded);
        }

        void ShowLoaded(object sender, RoutedEventArgs e)
        {
            Series show = Utilities.FetchShow(Id);
            ShowInfo.DataContext = show;
            SanitizedSummary = Utilities.StripAndFormatHTML(false, show.Summary);
            SummaryText.Text = SanitizedSummary;
            GenresText.Text = "Žanr: " + (String.Join(" ,", show.Genres.Take(3)));
            List<Season> results = Utilities.FetchSeasons(Id);
            foreach(var season in results)
            {
                season.CheckName();
            }
            seasonList.ItemsSource = results;
        }

        private void ButtonClicked(object sender, RoutedEventArgs e)
        {
            var button = sender as Grid;
            var code = ((Season)button.DataContext).Id;
            var title = ((Season)button.DataContext).Name;

            Episodes show_page = new Episodes(code, title);
            show_page.Show();
        }


    }
}
