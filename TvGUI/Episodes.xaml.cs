﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TvCore;
using TvCore.DataManagment;
using System.Linq;
using System.Collections;
namespace TvGUI
{
    /// <summary>
    /// Interaction logic for Episodes.xaml
    /// </summary>
    public partial class Episodes : Window
    {
        public int Id { get; set; }

        public Episodes(int id, string name)
        {
            this.Title = name + ", epizode";
            InitializeComponent();
            List<Episode> episodes = Utilities.FetchEpisodes(id);
            episodes = Utilities.TranslateType(episodes);
            episodeList.ItemsSource = episodes;
            EpTitle.Text = $"{name}, Epizode: ".ToUpper();
        }


    }
}
