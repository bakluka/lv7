﻿using System;
using TvCore;
using TvCore.DataManagment;
using System.Collections.Generic;
using System.Linq;

namespace TvConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Unesi trazeni naslov serije: ");
            var term = Console.ReadLine();
            List<Series> search_results = Utilities.SearchShows(term);
            
            var seasons = Utilities.FetchSeasons(search_results.First().Id);
            Console.WriteLine(seasons.Count);
            foreach(var season in seasons)
            {
                Console.WriteLine(season.Name+"\n");
                Console.WriteLine(season.Image.Original + "\n");
                Console.WriteLine(season.Summary+"\n");
            }

            List<Episode> episodes = Utilities.FetchEpisodes(seasons.First().Id);
            Console.WriteLine(episodes.First().Type);
            Console.WriteLine(episodes.First().Airdate);
        }

    }
}
