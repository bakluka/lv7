﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;


namespace TvCore
{
    public class Image
    {
        public Image()
        {
              
        }

        public Image(string path)
        {
            Medium = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Error.svg/1200px-Error.svg.png";
        }
        [JsonProperty("medium")]
        public string Medium { get; set; }

        [JsonProperty("original")]
        public string Original { get; set; }
    }
}
