﻿using System;
using TvCore;
using TvCore.DataManagment;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace TvCore.DataManagment
{
    public static class Utilities
    {
        public static Series FetchShow(int show_id)
        {
            using (WebClient web_client = new WebClient())
            {
                var json = web_client.DownloadString($"http://api.tvmaze.com/shows/{show_id}");
                Series new_show = JObject.Parse(json).ToObject<Series>();
                return new_show;
            }
        }

        public static List<Series> SearchShows(string title)
        {
            using (WebClient web_client = new WebClient())
            {
                var json = web_client.DownloadString($"http://api.tvmaze.com/search/shows?q={title}");
                JArray shows = JArray.Parse(json);
                if (!shows.HasValues)
                {
                    return null;
                }
                List<Series> result_shows = new List<Series>();
                foreach (var show in shows)
                {
                    Series new_show = show["show"].ToObject<Series>();
                    new_show.Summary = Utilities.StripAndFormatHTML(true, new_show.Summary);
                    result_shows.Add(new_show);
                }

                return result_shows;
            }
        }

        public static List<Season> FetchSeasons(int show_id)
        {
            using (WebClient web_client = new WebClient())
            {
                var json = web_client.DownloadString($"http://api.tvmaze.com/shows/{show_id}/seasons");
                JArray shows = JArray.Parse(json);
                if (!shows.HasValues)
                {
                    return null;
                }
                List<Season> result_seasons = new List<Season>();
                foreach (var season in shows)
                {
                    Season new_season = season.ToObject<Season>();
                    new_season.Summary = Utilities.StripAndFormatHTML(false, new_season.Summary);
                    result_seasons.Add(new_season);
                }

                return result_seasons;
            }
        }

        public static List<Episode> FetchEpisodes(int season_id)
        {
            using (WebClient web_client = new WebClient())
            {
                var json = web_client.DownloadString($"http://api.tvmaze.com/seasons/{season_id}/episodes");
                JArray episodes = JArray.Parse(json);
                if (!episodes.HasValues)
                {
                    return null;
                }
                List<Episode> result_seasons = new List<Episode>();
                foreach (var episode in episodes)
                {
                    Episode new_season = episode.ToObject<Episode>();
                    new_season.Summary = Utilities.StripAndFormatHTML(false, new_season.Summary);
                    result_seasons.Add(new_season);
                }

                return result_seasons;
            }
        }


        public static string StripAndFormatHTML<T>(bool shorten, T html_string)
        {
            if (html_string == null)
            {
                return "Nije dostupno";
            }
            string pattern = @"<(.|\n)*?>";

            string html_free = (Regex.Replace(html_string.ToString(), pattern, string.Empty)).Trim();
            if (html_free.Length > 200 && shorten){
                return html_free.Substring(0, 197)+"...";
            }
            return html_free;
        }

        public static List<Episode> TranslateType(List<Episode> episodes)
        {
            foreach (var ep in episodes.Where(e => e.Type != "regular")) ep.Type = "Specijal";
            foreach (var ep in episodes.Where(e=> e.Type == "regular")) ep.Type = "Standardna epizoda";
            return episodes;
        }
    }
}
