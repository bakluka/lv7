﻿    using System;
    using System.Collections.Generic;
    using System.Collections;
    using System.Text;
    using Newtonsoft.Json;

namespace TvCore
{
    public class Series
    {
        public Series()
        {
            Name = "Ne postoji serija takvog naslova!";
            Image = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Error.svg/1200px-Error.svg.png");
            Summary = " ";
            Id = -1;
        }


        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("genres")]
        public string[] Genres { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("premiered")]
        public string Premiered { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }
    }
}
