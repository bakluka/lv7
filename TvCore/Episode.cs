﻿using System;
using Newtonsoft.Json;

namespace TvCore
{
    public class Episode
    {
        public Episode()
        {

        }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("airdate")]
        public string Airdate { get; set; }
        [JsonProperty("runtime")]
        public int Runtime { get; set; }
        [JsonProperty("summary")]
        public string Summary { get; set; }


    }
}
