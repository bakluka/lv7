﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Newtonsoft.Json;


namespace TvCore
{
    public class Season
    {
        public Season()
        {

        }
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("number")]
        public int Number { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("premiereDate")]
        public string PremiereDate { get; set; }

        [JsonProperty("endDate")]
        public string EndDate { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }

        public void CheckName() //api rijetko vrati naziv sezone
        {
            if (string.IsNullOrEmpty(Name))
            {
               Name = $"{Number}. Sezona";
            }
        }
    }
}
